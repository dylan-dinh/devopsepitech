import Mongoose from "mongoose";
// import { UserModel } from "./users/users.model";

let database: Mongoose.Connection;


export const connect = () => {
  const username = process.env.MONGO_USERNAME || "root"
  const password = process.env.MONGO_PASSWORD || "rootroot"
  const dbname = process.env.MONGO_DATABASE || "test"
  const host = process.env.MONGO_HOST || "localhost";
  const port = process.env.MONGO_PORT || 27017;
  const containerName = process.env.CONTAINER_NAME || "db"
  const uri = process.env.URI
  Mongoose.connect(uri/*"mongodb://" + containerName + ":" + port + "/" + dbname*/, {
    useNewUrlParser: true,
    useFindAndModify: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
  }); database = Mongoose.connection; database.once("open", async () => {
    console.log("Connected to database");
  }); database.on("error", () => {
    console.log("Error connecting to database");
  });
};
export const disconnect = () => {
  if (!database) {
    return;
  } Mongoose.disconnect();
};

export const isConnected = () => {
  Mongoose.connection.on("error", error => {
    console.log("Database conn error TETS : ", error)
  })
}

const kittySchema = new Mongoose.Schema({
  name: String
});
const Kitten = Mongoose.model('Kitten', kittySchema);

export { Kitten };

